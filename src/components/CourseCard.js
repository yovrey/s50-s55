import {useState, useEffect} from "react"
import {Card, Button} from 'react-bootstrap';
import PropTypes from "prop-types"
import { Link } from "react-router-dom"

/*
export default function CourseCard(props) {
  return (
    <Card>
      <Card.Body>
        <Card.Title>{props.course.name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{props.course.description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>PhP {props.course.price}</Card.Text>
        <Button variant="primary">Enroll</Button>
      </Card.Body>
    </Card>
  );
}
*/

export default function CourseCard({course}) {

  // Destructuring the props
  const {name, description, price, _id} = course

  // Initialize a 'count' state with a value of 0
  const [count, setCount] = useState(0) 
  const [slots, setSlots] = useState(15)
  const [isOpen, setIsOpen] = useState(true)

/*  function enroll(){

    if(slots !== 0) {
      setCount(count + 1)
      setSlots(slots -1)
    }
    else {
      alert("No more slots available!")
    }
  }

  useEffect( () => {
      if(slots === 0) {
        setIsOpen(false)
      }
  }, [slots])*/

 
  return (
    <Card>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>PhP {price}</Card.Text>
        <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
      </Card.Body>
    </Card>
  );
}

// Prop Types can be used to validate the data coming from the props. You can define each property of the prop and assign specific validation for each of them.
CourseCard.propTypes = {
  course: PropTypes.shape({
      name: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired
  })
}

