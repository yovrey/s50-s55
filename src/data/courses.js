const courses_data = [
	{
		id: "wdc001",
		name: "PHP and Laravel",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi natus eos, enim quas nihil deleniti ea nisi ipsam hic sed impedit laborum pariatur quo, quos? Illo, quisquam. Tempora explicabo, dignissimos.",
		price: 40000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python and Django",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Javascript and Springboot",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
		price: 70000,
		onOffer: true
	},
]



export default courses_data